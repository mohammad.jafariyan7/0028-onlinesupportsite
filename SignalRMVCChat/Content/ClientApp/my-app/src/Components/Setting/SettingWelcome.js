import React from "react";
import { Alert } from "react-bootstrap";

const SettingWelcome = () => {
  return (
    <>
      <Alert variant={"warning"}>

        برای تنظیمات مورد نظر از منو انتخاب نمایید

      </Alert>
    </>
  );
};

export default SettingWelcome;
