import React, { Component } from "react";
import TelegramBotLayout from "./../TelegramBot/TelegramBotLayout";

export default class TelegramBotPage extends Component {
  render() {
    return (
      <div>
        <TelegramBotLayout />
      </div>
    );
  }
}
