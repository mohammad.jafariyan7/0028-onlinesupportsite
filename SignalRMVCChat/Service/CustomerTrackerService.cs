﻿using TelegramBotsWebApplication.Areas.Admin.Service;

namespace SignalRMVCChat.Service
{
    public class CustomerTrackerService:GenericService<CustomerTrackInfo>
    {
        public CustomerTrackerService() : base(null)
        {
        }
        
    }
}