﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRMVCChat.Service.Compaign
{
    public class CompaignTemplateSamples
    {

        public static string Sample1 = @"
<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>

    <!-- START HEADER/BANNER -->
    
            <tbody><tr>
                <td align='center'>
                    <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
                        <tbody><tr>
                            <td align='center' valign='top' background='https://designmodo.com/demo/emailtemplate/images/header-background.jpg' bgcolor='#66809b' style='background-size:cover; background-position:top;height=' 400''=''>
                                <table class='col-600' width='600' height='400' border='0' align='center' cellpadding='0' cellspacing='0'>
    
                                    <tbody><tr>
                                        <td height='40'></td>
                                    </tr>
    
    
                                    <tr>
                                        <td align='center' style='line-height: 0px;'>
                                            <img style='display:block; line-height:0px; font-size:0px; border:0px;' src='https://designmodo.com/demo/emailtemplate/images/logo.png' width='109' height='103' alt='logo'>
                                        </td>
                                    </tr>
    
    
    
                                    <tr>
                                        <td align='center' style='font-family: 'Raleway', sans-serif; font-size:37px; color:#ffffff; line-height:24px; font-weight: bold; letter-spacing: 7px;'>
                                            EMAIL <span style='font-family: 'Raleway', sans-serif; font-size:37px; color:#ffffff; line-height:39px; font-weight: 300; letter-spacing: 7px;'>TEMPLATE</span>
                                        </td>
                                    </tr>
    
    
    
    
    
                                    <tr>
                                        <td align='center' style='font-family: 'Lato', sans-serif; font-size:15px; color:#ffffff; line-height:24px; font-weight: 300;'>
                                            A creative email template for your email campaigns, promotions <br>and products across different email platforms.
                                        </td>
                                    </tr>
    
    
                                    <tr>
                                        <td height='50'></td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
    
    <!-- END HEADER/BANNER -->
    
    
    <!-- START 3 BOX SHOWCASE -->
    
            <tr>
                <td align='center'>
                    <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='margin-left:20px; margin-right:20px; border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;'>
                        <tbody><tr>
                            <td height='35'></td>
                        </tr>
    
                        <tr>
                            <td align='center' style='font-family: 'Raleway', sans-serif; font-size:22px; font-weight: bold; color:#2a3a4b;'>A creative way to showcase your content</td>
                        </tr>
    
                        <tr>
                            <td height='10'></td>
                        </tr>
    
    
                        <tr>
                            <td align='center' style='font-family: 'Lato', sans-serif; font-size:14px; color:#757575; line-height:24px; font-weight: 300;'>
                                Prepare some  flat icons of your choice. You can place your content below.<br>
                                Make sure it's awesome.
                            </td>
                        </tr>
    
                    </tbody></table>
                </td>
            </tr>
    
            <tr>
                <td align='center'>
                    <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9; '>
                        <tbody><tr>
                            <td height='10'></td>
                        </tr>
                        <tr>
                            <td>
    
    
                                <table class='col3' width='183' border='0' align='left' cellpadding='0' cellspacing='0'>
                                    <tbody><tr>
                                        <td height='30'></td>
                                    </tr>
                                    <tr>
                                        <td align='center'>
                                            <table class='insider' width='133' border='0' align='center' cellpadding='0' cellspacing='0'>
    
                                                <tbody><tr align='center' style='line-height:0px;'>
                                                    <td>
                                                        <img style='display:block; line-height:0px; font-size:0px; border:0px;' src='https://designmodo.com/demo/emailtemplate/images/icon-about.png' width='69' height='78' alt='icon'>
                                                    </td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='15'></td>
                                                </tr>
    
    
                                                <tr align='center'>
                                                    <td style='font-family: 'Raleway', Arial, sans-serif; font-size:20px; color:#2b3c4d; line-height:24px; font-weight: bold;'>About Us</td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='10'></td>
                                                </tr>
    
    
                                                <tr align='center'>
                                                    <td style='font-family: 'Lato', sans-serif; font-size:14px; color:#757575; line-height:24px; font-weight: 300;'>Place some cool text here.</td>
                                                </tr>
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='30'></td>
                                    </tr>
                                </tbody></table>
    
    
    
    
    
                                <table width='1' height='20' border='0' cellpadding='0' cellspacing='0' align='left'>
                                    <tbody><tr>
                                        <td height='20' style='font-size: 0;line-height: 0;border-collapse: collapse;'>
                                            <p style='padding-left: 24px;'>&nbsp;</p>
                                        </td>
                                    </tr>
                                </tbody></table>
    
    
    
                                <table class='col3' width='183' border='0' align='left' cellpadding='0' cellspacing='0'>
                                    <tbody><tr>
                                        <td height='30'></td>
                                    </tr>
                                    <tr>
                                        <td align='center'>
                                            <table class='insider' width='133' border='0' align='center' cellpadding='0' cellspacing='0'>
    
                                                <tbody><tr align='center' style='line-height:0px;'>
                                                    <td>
                                                        <img style='display:block; line-height:0px; font-size:0px; border:0px;' src='https://designmodo.com/demo/emailtemplate/images/icon-team.png' width='69' height='78' alt='icon'>
                                                    </td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='15'></td>
                                                </tr>
    
    
                                                <tr align='center'>
                                                    <td style='font-family: 'Raleway', sans-serif; font-size:20px; color:#2b3c4d; line-height:24px; font-weight: bold;'>Our Team</td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='10'></td>
                                                </tr>
    
    
                                                <tr align='center'>
                                                        <td style='font-family: 'Lato', sans-serif; font-size:14px; color:#757575; line-height:24px; font-weight: 300;'>Place some cool text here.</td>
                                                </tr>
    
    
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='30'></td>
                                    </tr>
                                </tbody></table>
    
    
    
                                <table width='1' height='20' border='0' cellpadding='0' cellspacing='0' align='left'>
                                    <tbody><tr>
                                        <td height='20' style='font-size: 0;line-height: 0;border-collapse: collapse;'>
                                            <p style='padding-left: 24px;'>&nbsp;</p>
                                        </td>
                                    </tr>
                                </tbody></table>
    
    
    
                                <table class='col3' width='183' border='0' align='right' cellpadding='0' cellspacing='0'>
                                    <tbody><tr>
                                        <td height='30'></td>
                                    </tr>
                                    <tr>
                                        <td align='center'>
                                            <table class='insider' width='133' border='0' align='center' cellpadding='0' cellspacing='0'>
    
                                                <tbody><tr align='center' style='line-height:0px;'>
                                                    <td>
                                                        <img style='display:block; line-height:0px; font-size:0px; border:0px;' src='https://designmodo.com/demo/emailtemplate/images/icon-portfolio.png' width='69' height='78' alt='icon'>
                                                    </td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='15'></td>
                                                </tr>
    
    
                                                <tr align='center'>
                                                    <td style='font-family: 'Raleway',  sans-serif; font-size:20px; color:#2b3c4d; line-height:24px; font-weight: bold;'>Our Portfolio</td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='10'></td>
                                                </tr>
    
    
                                                <tr align='center'>
                                                    <td style='font-family: 'Lato', sans-serif; font-size:14px; color:#757575; line-height:24px; font-weight: 300;'>Place some cool text here.</td>
                                                </tr>
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='30'></td>
                                    </tr>
                                </tbody></table>
    
    
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
                <tr>
                        <td height='5'></td>
            </tr>
    
    
    <!-- END 3 BOX SHOWCASE -->
    
    
    <!-- START AWESOME TITLE -->
    
            <tr>
                <td align='center'>
                    <table align='center' class='col-600' width='600' border='0' cellspacing='0' cellpadding='0'>
                        <tbody><tr>
                            <td align='center' bgcolor='#2a3b4c'>
                                <table class='col-600' width='600' align='center' border='0' cellspacing='0' cellpadding='0'>
                                    <tbody><tr>
                                        <td height='33'></td>
                                    </tr>
                                    <tr>
                                        <td>
    
    
                                            <table class='col1' width='183' border='0' align='left' cellpadding='0' cellspacing='0'>
    
                                                <tbody><tr>
                                                <td height='18'></td>
                                                </tr>
    
                                                <tr>
                                                    <td align='center'>
                                                        <img style='display:block; line-height:0px; font-size:0px; border:0px;' class='images_style' src='https://designmodo.com/demo/emailtemplate/images/icon-title.png' alt='img' width='156' height='136'>
                                                    </td>
    
    
    
                                                </tr>
                                            </tbody></table>
    
    
    
                                            <table class='col3_one' width='380' border='0' align='right' cellpadding='0' cellspacing='0'>
    
                                                <tbody><tr align='left' valign='top'>
                                                    <td style='font-family: 'Raleway', sans-serif; font-size:20px; color:#f1c40f; line-height:30px; font-weight: bold;'>This title is definitely awesome! </td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='5'></td>
                                                </tr>
    
    
                                                <tr align='left' valign='top'>
                                                    <td style='font-family: 'Lato', sans-serif; font-size:14px; color:#fff; line-height:24px; font-weight: 300;'>
                                                        The use of flat colors in web design is more than a recent trend, it is a style designers have used for years to create impactful visuals. When you hear 'flat', it doesn't mean boring it just means minimalist.
                                                    </td>
                                                </tr>
    
                                                <tr>
                                                    <td height='10'></td>
                                                </tr>
    
                                                <tr align='left' valign='top'>
                                                    <td>
                                                        <table class='button' style='border: 2px solid #fff;' bgcolor='#2b3c4d' width='30%' border='0' cellpadding='0' cellspacing='0'>
                                                            <tbody><tr>
                                                                <td width='10'></td>
                                                                <td height='30' align='center' style='font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#ffffff;'>
                                                                    <a href='#' style='color:#ffffff;'>Read more</a>
                                                                </td>
                                                                <td width='10'></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='33'></td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
    
    <!-- END AWESOME TITLE -->
    
    
    <!-- START WHAT WE DO -->
    
            <tr>
                <td align='center'>
                    <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='margin-left:20px; margin-right:20px;'>
    
    
    
            <tbody><tr>
                <td align='center'>
                    <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style=' border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;'>
                        <tbody><tr>
                            <td height='50'></td>
                        </tr>
                        <tr>
                            <td align='right'>
    
    
                                <table class='col2' width='287' border='0' align='right' cellpadding='0' cellspacing='0'>
                                    <tbody><tr>
                                        <td align='center' style='line-height:0px;'>
                                            <img style='display:block; line-height:0px; font-size:0px; border:0px;' class='images_style' src='https://designmodo.com/demo/emailtemplate/images/icon-responsive.png' width='169' height='138'>
                                        </td>
                                    </tr>
                                </tbody></table>
    
    
    
    
    
    
                                <table width='287' border='0' align='left' cellpadding='0' cellspacing='0' class='col2' style=''>
                                    <tbody><tr>
                                        <td align='center'>
                                            <table class='insider' width='237' border='0' align='center' cellpadding='0' cellspacing='0'>
    
    
    
                                                <tbody><tr align='left'>
                                                    <td style='font-family: 'Raleway', sans-serif; font-size:23px; color:#2a3b4c; line-height:30px; font-weight: bold;'>What we do?</td>
                                                </tr>
    
                                                <tr>
                                                    <td height='5'></td>
                                                </tr>
    
    
                                                <tr>
                                                    <td style='font-family: 'Lato', sans-serif; font-size:14px; color:#7f8c8d; line-height:24px; font-weight: 300;'>
                                                        We create responsive websites with modern designs and features for small businesses and organizations that are professionally developed and SEO optimized.
                                                    </td>
                                                </tr>
    
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
    
    <!-- END WHAT WE DO -->
    
    
    
    <!-- START READY FOR NEW PROJECT -->
    
            <tr>
                <td align='center'>
                    <table align='center' width='100%' border='0' cellspacing='0' cellpadding='0' style=' border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;'>
                        <tbody><tr>
                            <td height='50'></td>
                        </tr>
                        <tr>
    
    
                            <td align='center' bgcolor='#34495e'>
                                <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
                                    <tbody><tr>
                                        <td height='35'></td>
                                    </tr>
    
    
                                    <tr>
                                        <td align='center' style='font-family: 'Raleway', sans-serif; font-size:20px; color:#f1c40f; line-height:24px; font-weight: bold;'>Ready for new project?</td>
                                    </tr>
    
    
                                    <tr>
                                        <td height='20'></td>
                                    </tr>
    
    
                                    <tr>
                                        <td align='center' style='font-family: 'Lato', sans-serif; font-size:14px; color:#fff; line-height: 1px; font-weight: 300;'>
                                            Check out our price below.
                                        </td>
                                    </tr>
    
    
                                    <tr>
                                        <td height='40'></td>
                                    </tr>
    
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
    
    <!-- END READY FOR NEW PROJECT -->
    
    
    <!-- START PRICING TABLE -->
    
            <tr>
                <td align='center'>
                    <table width='600' class='col-600' align='center' border='0' cellspacing='0' cellpadding='0' style=' border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;'>
                        <tbody><tr>
                            <td height='50'></td>
                        </tr>
                        <tr>
                            <td>
    
    
                                <table style='border:1px solid #e2e2e2;' class='col2' width='287' border='0' align='left' cellpadding='0' cellspacing='0'>
    
    
                                    <tbody><tr>
                                        <td height='40' align='center' bgcolor='#2b3c4d' style='font-family: 'Raleway', sans-serif; font-size:18px; color:#f1c40f; line-height:30px; font-weight: bold;'>Small Business Website</td>
                                    </tr>
    
    
                                    <tr>
                                        <td align='center'>
                                            <table class='insider' width='237' border='0' align='center' cellpadding='0' cellspacing='0'>
                                                <tbody><tr>
                                                    <td height='20'></td>
                                                </tr>
    
                                                <tr align='center' style='line-height:0px;'>
                                                    <td style='font-family: 'Lato', sans-serif; font-size:48px; color:#2b3c4d; font-weight: bold; line-height: 44px;'>$150</td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='15'></td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='15'></td>
                                                </tr>
    
    
    
                                                <tr>
                                                    <td align='center'>
                                                        <table width='100' border='0' align='center' cellpadding='0' cellspacing='0' style='border: 2px solid #2b3c4d;'>
                                                            <tbody><tr>
                                                                <td width='10'></td>
                                                                <td height='30' align='center' style='font-family: 'Lato', sans-serif; font-size:14px; font-weight: 300; color:#2b3c4d;'>
                                                                    <a href='#' style='color: #2b3c4d;'>Learn More</a>
                                                                </td>
                                                                <td width='10'></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
    
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='30'></td>
                                    </tr>
                                </tbody></table>
    
    
    
    
    
                                <table width='1' height='20' border='0' cellpadding='0' cellspacing='0' align='left'>
                                    <tbody><tr>
                                        <td height='20' style='font-size: 0;line-height: 0;border-collapse: collapse;'>
                                            <p style='padding-left: 24px;'>&nbsp;</p>
                                        </td>
                                    </tr>
                                </tbody></table>
    
    
                                <table style='border:1px solid #e2e2e2;' class='col2' width='287' border='0' align='right' cellpadding='0' cellspacing='0'>
    
    
                                    <tbody><tr>
                                        <td height='40' align='center' bgcolor='#2b3c4d' style='font-family: 'Raleway', sans-serif; font-size:18px; color:#f1c40f; line-height:30px; font-weight: bold;'>E-commerce Website</td>
                                    </tr>
    
    
                                    <tr>
                                        <td align='center'>
                                            <table class='insider' width='237' border='0' align='center' cellpadding='0' cellspacing='0'>
                                                <tbody><tr>
                                                    <td height='20'></td>
                                                </tr>
    
                                                <tr align='center' style='line-height:0px;'>
                                                    <td style='font-family: 'Lato', sans-serif; font-size:48px; color:#2b3c4d; font-weight: bold; line-height: 44px;'>$289</td>
                                                </tr>
    
    
                                                <tr>
                                                    <td height='30'></td>
                                                </tr>
    
    
    
                                                <tr align='center'>
                                                    <td>
                                                        <table width='100' border='0' align='center' cellpadding='0' cellspacing='0' style=' border: 2px solid #2b3c4d;'>
                                                            <tbody><tr>
                                                                <td width='10'></td>
                                                                <td height='30' align='center' style='font-family: 'Lato', sans-serif; font-size:14px; font-weight: 300; color:#2b3c4d;'>
                                                                    <a href='#' style='color: #2b3c4d;'>Learn More</a>
                                                                </td>
                                                                <td width='10'></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
    
    
                                            </tbody></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='20'></td>
                                    </tr>
                                </tbody></table>
    
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
    
    <!-- END PRICING TABLE -->
    
    
    <!-- START FOOTER -->
    
            <tr>
                <td align='center'>
                    <table align='center' width='100%' border='0' cellspacing='0' cellpadding='0' style=' border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;'>
                        <tbody><tr>
                            <td height='50'></td>
                        </tr>
                        <tr>
                            <td align='center' bgcolor='#34495e' background='https://designmodo.com/demo/emailtemplate/images/footer.jpg' height='185'>
                                <table class='col-600' width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
                                    <tbody><tr>
                                        <td height='25'></td>
                                    </tr>
    
                                        <tr>
                                        <td align='center' style='font-family: 'Raleway',  sans-serif; font-size:26px; font-weight: 500; color:#f1c40f;'>Follow us for some cool stuffs</td>
                                        </tr>
    
    
                                    <tr>
                                        <td height='25'></td>
                                    </tr>
    
    
    
                                    </tbody></table><table align='center' width='35%' border='0' cellspacing='0' cellpadding='0'>
                                    <tbody><tr>
                                        <td align='center' width='30%' style='vertical-align: top;'>
                                                <a href='https://www.facebook.com/designmodo' target='_blank'> <img src='https://designmodo.com/demo/emailtemplate/images/icon-fb.png'> </a>
                                        </td>
    
                                        <td align='center' class='margin' width='30%' style='vertical-align: top;'>
                                             <a href='https://twitter.com/designmodo' target='_blank'> <img src='https://designmodo.com/demo/emailtemplate/images/icon-twitter.png'> </a>
                                        </td>
    
                                        <td align='center' width='30%' style='vertical-align: top;'>
                                                <a href='https://plus.google.com/+Designmodo/posts' target='_blank'> <img src='https://designmodo.com/demo/emailtemplate/images/icon-googleplus.png'> </a>
                                        </td>
                                    </tr>
                                    </tbody></table>
    
    
    
                                </td></tr></tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    
    <!-- END FOOTER -->
    
                            
                        
                    </tbody></table>";
        public static string Sample2 = @"<head>
    <title>A Responsive Email Template</title>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <style type='text/css'>
      /* CLIENT-SPECIFIC STYLES */
      #outlook a{padding:0;} /* Force Outlook to provide a 'view in browser' message */
      .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
      .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
      body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
      table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
      img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
      /* RESET STYLES */
      body{margin:0; padding:0;}
      img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
      table{border-collapse:collapse !important;}
      body{height:100% !important; margin:0; padding:0; width:100% !important;}
      /* iOS BLUE LINKS */
      .appleBody a {color:#68440a; text-decoration: none;}
      .appleFooter a {color:#999999; text-decoration: none;}
      /* MOBILE STYLES */
      @media screen and (max-width: 666px) {
      /* ALLOWS FOR FLUID TABLES */
      /*table[class='wrapper']*/
      /* ADJUSTS LAYOUT OF LOGO IMAGE */
      td[class='logo']{
      text-align: left;
      padding: 20px 0 20px 0 !important;
      }
      td[class='logo'] img{
      margin:0 auto!important;
      }
      /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
      td[class='mobile-hide']{
      display:none;}
      img[class='mobile-hide']{
      display: none !important;
      }
      /*img[class='img-max']*/
      /* FULL-WIDTH TABLES */
      /*table[class='responsive-table']*/
      /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
      td[class='padding']{
      padding: 10px 5% 15px 5% !important;
      }
      /*td[class='padding-copy']*/
      td[class='padding-meta']{
      padding: 30px 5% 0px 5% !important;
      text-align: center;
      }
      td[class='no-pad']{
      padding: 0 0 20px 0 !important;
      }
      td[class='no-padding']{
      padding: 0 !important;
      }
      /*td[class='section-padding']*/
      td[class='section-padding-bottom-image']{
      padding: 50px 15px 0 15px !important;
      }
      /* ADJUST BUTTONS ON MOBILE */
      /*td[class='mobile-wrapper']*/
      table[class='mobile-button-container']{
      margin:0 auto;
      width:100% !important;
      }
      a[class='mobile-button']{
      width:80% !important;
      padding: 15px !important;
      border: 0 !important;
      font-size: 16px !important;
      }
      }
    </style>
  </head>
  
  <body style='margin: 0; padding: 0;'>
    <!-- HEADER -->
    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
      <tbody>
        <tr>
          <td bgcolor='#FFFFFF'>
            <!-- HIDDEN PREHEADER TEXT -->
            <div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>
              <p>Everyday chores just got a lot easier.</p>
              ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp;
              ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp;
              ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp;
              ‌&nbsp; ‌&nbsp; ‌&nbsp; ‌&nbsp;
            </div>
            <div align='center' style='padding: 0px 15px 0px 15px;'>
              <table border='0' cellpadding='0' cellspacing='0' width='666' class='wrapper'>
                <!-- LOGO/PREHEADER TEXT -->
                <tbody>
                  <tr>
                    <td style='padding: 30px 0px 20px 0px;' class='logo'>
                      <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                        <tbody>
                          <tr bgcolor='#FFFFFF'>
                            <td>
                              <center>
                                <a href='https://www.taskrabbit.com?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' target='_blank'><img src='http://media.sailthru.com/53p/1k0/6/d/575f4958dbe38.png' width='200' height='30'></a>
                              </center>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  
  
    <!-- ONE COLUMN W/IMG SECTION -->
    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
      <tbody>
        <tr>
          <td bgcolor='#ffffff' align='center' style='padding: 0px 15px 18px 15px;'>
            <table border='0' cellpadding='0' cellspacing='0' width='666' class='responsive-table'>
              <tbody>
                <tr>
                  <td>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                      <tbody>
                        <tr>
                          <td style='background-image: url(http://media.sailthru.com/53p/1k0/2/p/56cf3fd48c073.png); background-position: top center; height: 31px; background-repeat: no-repeat;'>
                            <a href='https://www.taskrabbit.com/m/cleaning?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' 'https:=' ' www.taskrabbit.co.uk=' ' m=' ' cleaning'='' target='_blank' style='float: left; display: block; padding-left:0; width:110px; height:20px;'></a>
                            <a href='https://www.taskrabbit.com/m/shopping-delivery?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' 'https:=' ' www.taskrabbit.co.uk=' ' m=' ' shopping-delivery'='' target='_blank' style='float: left; display: block; padding-left:34px; width:70px; height:20px;'></a>
                            <a href='https://www.taskrabbit.com/m/handyman?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' 'https:=' ' www.taskrabbit.co.uk=' ' m=' ' handyman'='' target='_blank' style='float: left; display: block; padding-left:34px; width:115px; height:20px;'></a>
                            <a href='https://www.taskrabbit.com/m/moving-help?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' 'https:=' ' www.taskrabbit.co.uk=' ' m=' ' moving-help'='' target='_blank' style='float: left; display: block; padding-left:34px; width:150px; height:20px;'></a>
                            <a href='https://www.taskrabbit.com/m/gen-help?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' 'https:=' ' www.taskrabbit.co.uk=' ' m=' ' gen-help'='' target='_blank' style='float: left; display: block; padding-left:34px; width:80px; height:20px;'></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                      <tbody>
                        <tr>
                          <td align='center' style='padding: 18px 0 0 0; border-top: 1px #CFD2D3 solid;'>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- HERO IMAGE -->
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                      <tbody>
                        <tr>
                          <td>
                            <a href='https://itunes.apple.com/us/app/id374165361?mt=8' target='_blank'>
                              <img src='http://media.sailthru.com/53p/1k0/7/p/57967e61e0738.jpg' width='666' border='0' alt='TaskRabbit Panel 1' style='display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;' class='img-max'>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href='https://www.taskrabbit.com/m/cleaning?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' target='_blank'>
                              <img src='http://media.sailthru.com/53p/1k0/7/c/578579609a9ab.jpg' width='666' border='0' alt='TaskRabbit Panel 2' style='display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;' class='img-max'>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href='https://www.taskrabbit.com/m/handyman?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' target='_blank'>
                              <img src='http://media.sailthru.com/53p/1k0/7/c/57857969bdec6.jpg' width='666' border='0' alt='TaskRabbit Panel 3' style='display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;' class='img-max'>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href='https://www.taskrabbit.com/m/shopping-delivery?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' target='_blank'>
                              <img src='http://media.sailthru.com/53p/1k0/7/c/578579747145e.jpg' width='666' border='0' alt='TaskRabbit Panel 4' style='display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;' class='img-max'>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href='https://www.taskrabbit.com?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' target='_blank'>
                              <img src='http://media.sailthru.com/53p/1k0/7/c/5785797ea4b26.jpg' width='666' border='0' alt='TaskRabbit Panel 5' style='display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;' class='img-max'>
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <!-- BEGIN HR -->
        <tr>
          <td align='center' style='padding: 0 0px 0 0px;'>
            <!--<td align='center' style='padding: 0 15px 0 15px;'>-->
            <table border='0' cellpadding='0' cellspacing='0' width='666' class='responsive-table'>
              <tbody>
                <tr>
                  <td>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                      <tbody>
                        <tr>
                          <td align='center' style='padding: 0 0 0 0; border-bottom: 1px #CFD2D3 solid;'></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
          <!-- END HR -->
        </tr>
      </tbody>
    </table>
  
  
    <!-- APP DOWNLOAD -->
    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
      <tbody>
        <tr>
          <td align='center'>
            <table border='0' cellpadding='0' cellspacing='0' width='666' class='responsive-table'>
              <tbody>
                <tr>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td align='center'>
                    <table width='140' height='27' border='0' cellspacing='0' cellpadding='0' align='center' class='responsive-table'>
                      <tbody>
                        <tr>
                          <td align='center' style='padding-top: 36px; padding-bottom:36px;'>
                            <table width='140' height='27' border='0' cellspacing='0' cellpadding='0' align='center'>
                              <tbody>
                                <tr align='center'>
                                  <td width='33'>
                                    <a href='https://twitter.com/taskrabbit/?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' border='0' target='_blank'><img src='http://media.sailthru.com/53p/1jz/a/e/561ebb2dcaf58.png' width='33' height='27' style='display: block;'></a>
                                  </td>
                                  <td width='26'>
                                    <a href='https://www.instagram.com/taskrabbit/?utm_campaign=NEW+Welcome+Email+1&amp;utm_medium=email&amp;utm_source=Sailthru' border='0' target='_blank'><img src='http://media.sailthru.com/53p/1jz/a/e/561ebb81140eb.png' width='26' height='27' style='display: block;'></a>
                                  </td>
                                  <td width='26'>
                                    <a href='https://www.facebook.com/TaskRabbit/?utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' border='0' target='_blank'><img src='http://media.sailthru.com/53p/1jz/a/e/561ebb4f90bfa.png' width='26' height='27' style='display: block;'></a>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
  
                <tr>
                  <td>
                  </td>
                </tr>
                <tr align='center'>
                  <td>
                    <table width='140' height='27' border='0' cellspacing='0' cellpadding='0' align='center'>
                      <tbody>
                        <tr>
                          <td width='130' style='padding-right:10px;'>
                            <a href='https://itunes.apple.com/us/app/id374165361?mt=8' target='_blank'><img src='http://media.sailthru.com/53p/1jz/a/e/561ebac54feca.png' width='130' height='40' style='display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 130px; height: 40px;' alt='TaskRabbit in the AppStore'
                                border='0'></a>
                          </td>
                          <td width='130' style='padding-left:10px;'>
                            <a href='https://play.google.com/store/apps/details?id=com.taskrabbit.droid.consumer&amp;utm_source=Sailthru&amp;utm_medium=email&amp;utm_campaign=NEW%20Welcome%20Email%201' target='_blank'><img src='http://media.sailthru.com/53p/1jz/a/e/561ebafd84f13.png' width='130' height='40' style='display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 130px; height: 40px;' alt='TaskRabbit in the Google Play Store'
                                border='0'></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
  
  
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <!-- FOOTER -->
    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
      <tbody>
        <tr>
          <td bgcolor='#ffffff' align='center' style='padding: 36px 0;'>
            <!-- UNSUBSCRIBE COPY -->
            <table width='666' border='0' cellspacing='0' cellpadding='0' align='center' class='responsive-table'>
              <tbody>
                <tr>
                  <td align='center' style='font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;'>
                    <span class='appleFooter' style='color:#666666;'>TaskRabbit - San Francisco, CA</span><br><a href='#' class='original-only' style='color: #666666; text-decoration: none;'
                      target='_blank'>Unsubscribe</a>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<a href='https://support.taskrabbit.com/hc/en-us' class='original-only' style='color: #666666; text-decoration: none;' target='_blank'>Support</a><span class='original-only'
                      style='font-family: Arial, sans-serif; font-size: 12px; color: #444444;'>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;</span><a href='https://www.taskrabbit.com/privacy' style='color: #666666; text-decoration: none;' target='_blank'>Privacy Policy</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  
  </body>";
        public static string Sample3 = @"<style>

body {
	  width: 100%;
	  background-color: #ffffff;
	  margin: 0;
	  padding: 0;
	  -webkit-font-smoothing: antialiased;
	  font-family: Georgia, Times, serif
	}
	
	table {
	  border-collapse: collapse;
	}
	
	td#logo {
	  margin: 0 auto;
	  padding: 14px 0;
	}
	
	img {
	  border: none;
	  display: block;
	}
	
	a.blue-btn {
	  display: inline-block;
	  margin-bottom: 34px;
	  border: 3px solid #3baaff;
	  padding: 11px 38px;
	  font-size: 12px;
	  font-family: arial;
	  font-weight: bold;
	  color: #3baaff;
	  text-decoration: none;
	  text-align: center;
	}
	
	a.blue-btn:hover {
	  background-color: #3baaff;
	  color: #fff;
	}
	
	a.white-btn {
	  display: inline-block;
	  margin-bottom: 30px;
	  border: 3px solid #fff;
	  background: transparent;
	  padding: 11px 38px;
	  font-size: 12px;
	  font-family: arial;
	  font-weight: bold;
	  color: #fff;
	  text-decoration: none;
	  text-align: center;
	}
	
	a.white-btn:hover {
	  background-color: #fff;
	  color: #3baaff;
	}
	
	.border-complete {
	  border-top: 1px solid #dadada;
	  border-left: 1px solid #dadada;
	  border-right: 1px solid #dadada;
	}
	
	.border-lr {
	  border-left: 1px solid #dadada;
	  border-right: 1px solid #dadada;
	}
	
	#banner-txt {
	  color: #fff;
	  padding: 15px 32px 0px 32px;
	  font-family: arial;
	  font-size: 13px;
	  text-align: center;
	}
	
	h2#our-products {
	  font-family: 'Pacifico';
	  margin: 23px auto 5px auto;
	  font-size: 27px;
	  color: #3baaff;
	}
	
	h3.our-products {
	  font-family: arial;
	  font-size: 15px;
	  color: #7c7b7b;
	}
	
	p.our-products {
	  text-align: center;
	  font-family: arial;
	  color: #7c7b7b;
	  font-size: 12px;
	  padding: 10px 10px 24px 10px;
	}
	
	h2.special {
	  margin: 0;
	  color: #fff;
	  color: #fff;
	  font-family: 'Pacifico';
	  padding: 15px 32px 0px 32px;
	}
	
	p.special {
	  color: #fff;
	  font-size: 12px;
	  color: #fff;
	  text-align: center;
	  font-family: arial;
	  padding: 0px 32px 10px 32px;
	}
	
	h2#coupons {
	  color: #3baaff;
	  text-align: center;
	  font-family: 'Pacifico';
	  margin-top: 30px;
	}
	
	p#coupons {
	  color: #7c7b7b;
	  text-align: center;
	  font-size: 12px;
	  text-align: center;
	  font-family: arial;
	  padding: 0 32px;
	}
	
	#socials {
	  padding-top: 12px;
	}
	
	p#footer-txt {
	  text-align: center;
	  color: #303032;
	  font-family: arial;
	  font-size: 12px;
	  padding: 0 32px;
	}
	
	#social-icons {
	  width: 28%;
	}
	
	@media only screen and (max-width: 640px) {
	  body[yahoo] .deviceWidth {
	    width: 440px!important;
	    padding: 0;
	  }
	  body[yahoo] .center {
	    text-align: center!important;
	  }
	  #social-icons {
	    width: 40%;
	  }
	}
	
	@media only screen and (max-width: 479px) {
	  body[yahoo] .deviceWidth {
	    width: 280px!important;
	    padding: 0;
	  }
	  body[yahoo] .center {
	    text-align: center!important;
	  }
	  #social-icons {
	    width: 60%;
	  }
	}
</style>

<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' yahoo='fix' style='font-family: Georgia, Times, serif'>

    <!-- Wrapper -->
    <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
  
  
      <!-- Start Header-->
      <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-complete deviceWidth' bgcolor='#e9e9e9'>
        <tr>
          <td width='100%'>
            <!-- Logo -->
            <table border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
              <tr>
                <td id='logo' align='center'>
                  <a href='#'><img src='http://1stwebdesigner.com/demos/responsive-email-template/img/logo.png' alt='' border='0' /></a>
                </td>
              </tr>
            </table>
            <!-- End Logo -->
          </td>
        </tr>
      </table>
  
      <!-- End Header -->
  
  
      <!-- Image Banner -->
      <table width='600' class='border-lr deviceWidth' border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='#eeeeed'>
        <tr>
          <td>
            <a href='#'><img class='deviceWidth' src='http://lorempixel.com/600/253/' alt='' border='0' /></a>
          </td>
        </tr>
  
      </table>
      <!-- End Image Banner -->
  
  
      <!-- Banner Text -->
      <table width='600' height='108' border='0' cellpadding='0' cellspacing='0' align='center' class='border-lr deviceWidth' bgcolor='#3baaff'>
        <tr>
          <td align='center'>
            <p id='banner-txt'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            <a href='#' class='white-btn' align='center'> LEARN MORE</a>
          </td>
        </tr>
  
  
      </table>
      <!-- End of Banner Text -->
  
  
      <!-- Our Products -->
      <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-lr deviceWidth' bgcolor='#fff'>
        <tr>
          <td align='center'>
            <h2 id='our-products'>Our Products </h2> </td>
        </tr>
        <tr>
          <td class='center'>
  
            <table width='48%' border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth'>
  
              <tr>
                <td align='center'>
                  <h3 class='our-products'>Product No. 1 </h3>
                  <a href='#'><img width='262' src='http://1stwebdesigner.com/demos/responsive-email-template/img/image1.jpg' alt='' border='0' class='deviceWidth' /></a>
                  </p>
                  <p class='our-products'> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </td>
              </tr>
  
            </table>
  
            <table width='48%' border='0' cellpadding='0' cellspacing='0' align='right' class='deviceWidth'>
              <tr>
                <td align='center'>
                  <h3 class='our-products'>Product No. 2 </h3>
                  <a href='#'><img width='262' src='http://1stwebdesigner.com/demos/responsive-email-template/img/image1.jpg' alt='' border='0' class='deviceWidth' /></a>
                  </p>
                  <p class='our-products'> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </td>
              </tr>
  
            </table>
  
          </td>
        </tr>
      </table>
      <!-- End of Our Products -->
  
  
      <!-- Special Offer -->
      <table width='600' height='151' cellpadding='0' cellspacing='0' align='center' class='border-lr deviceWidth' bgcolor='#3baaff'>
  
        <tr>
          <td align='center'>
            <h2 class='special'>Special Offer </h2>
            <p class='special'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </td>
        </tr>
      </table>
      <!-- End of Special Offer -->
  
  
      <!--Coupons -->
      <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-lr deviceWidth' bgcolor='#fff'>
  
        <tr>
          <td style='text-align: center;'>
            <h2 id='coupons'> Check our site for coupons </h2>
            <p id='coupons'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
            <a href='#' class='blue-btn' align='center'> LEARN MORE</a>
          </td>
        </tr>
      </table>
      <!-- End of Coupons-->
  
  
      <!-- Footer -->
      <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-complete deviceWidth' bgcolor='#eeeeed'>
        <tr>
          <td align='center' valign='top' id='socials'>
            <table id='social-icons' border='0' cellspacing='0' cellpadding='0'>
              <tr>
                <td>
                  <a href='#'><img src='http://1stwebdesigner.com/demos/responsive-email-template/img/fb.png' width='32' height='32' style='display:block;' /></a>
                </td>
                <td>
                  <a href='#'><img src='http://1stwebdesigner.com/demos/responsive-email-template/img/twitter.png' width='32' height='32' style='display:block;' /></a>
                </td>
                <td>
                  <a href='#'><img src='http://1stwebdesigner.com/demos/responsive-email-template/img/pinterest.png' width='32' height='32' style='display:block;' /></a>
                </td>
                <td>
                  <a href='#'><img src='http://1stwebdesigner.com/demos/responsive-email-template/img/google.png' width='32' height='32' style='display:block;' /></a>
                </td>
  
              </tr>
            </table>
          </td>
        </tr>
  
        <tr>
          <td style='text-align: center;'>
            <p id='footer-txt'> <b>© Copyright 2013 - Email Template - All Rights Reserved</b>
              <br/> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
            </p>
          </td>
        </tr>
      </table>
      <!-- End of Footer-->
    </table>
    <!-- End Wrapper -->
  </body>";

        public static string Sample4 = @"<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
  <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0;'>
  <meta name='format-detection' content='telephone=no'/>

  <!-- Responsive Mobile-First Email Template by Konstantin Savchenko, 2015.
  https://github.com/konsav/email-templates/  -->

  <style>
/* Reset styles */ 
body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important;}
body, table, td, div, p, a { -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; border-spacing: 0; }
img { border: 0; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
#outlook a { padding: 0; }
.ReadMsgBody { width: 100%; } .ExternalClass { width: 100%; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

/* Extra floater space for advanced mail clients only */ 
@media all and (max-width: 600px) {
  .floater { width: 320px; }
}

/* Set color for auto links (addresses, dates, etc.) */ 
a, a:hover {
  color: #127DB3;
}
.footer a, .footer a:hover {
  color: #999999;
}

  </style>

  <!-- MESSAGE SUBJECT -->
  <title>Get this responsive email template</title>

</head>

<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin='0' rightmargin='0' bottommargin='0' leftmargin='0' marginwidth='0' marginheight='0' width='100%' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
  background-color: #FFFFFF;
  color: #000000;'
  bgcolor='#FFFFFF'
  text='#000000'>

<!-- SECTION / BACKGROUND -->
<!-- Set section background color -->
<table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;' class='background'><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
  bgcolor='#127DB3'>

<!-- WRAPPER -->
<!-- Set wrapper width (twice) -->
<table border='0' cellpadding='0' cellspacing='0' align='center'
  width='600' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
  max-width: 600px;' class='wrapper'>

  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
      padding-top: 20px;'>

      <!-- PREHEADER -->
      <!-- Set text color to background color -->
      <div style='display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
      color: #FFFFFF;' class='preheader'>
        Available on&nbsp;GitHub and&nbsp;CodePen. Highly compatible. Designer friendly. More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;— a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.</div>

      <!-- LOGO -->
      <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
      <a target='_blank' style='text-decoration: none;'
        href='https://github.com/konsav/email-templates/'><img border='0' vspace='0' hspace='0'
        src='https://raw.githubusercontent.com/konsav/email-templates/master/images/logo-white.png'
        width='100' height='30'
        alt='Logo' title='Logo' style='
        color: #000000;
        font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;' /></a>

    </td>
  </tr>

  <!-- HEADER -->
  <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
      padding-top: 20px;
      color: #FFFFFF;
      font-family: sans-serif;' class='header'>
        Explore responsive email templates
    </td>
  </tr>

  <!-- SUBHEADER -->
  <!-- Set text color and font family ('sans-serif' or 'Georgia, serif') -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
      padding-top: 5px;
      color: #FFFFFF;
      font-family: sans-serif;' class='subheader'>
        Available on&nbsp;GitHub and&nbsp;CodePen
    </td>
  </tr>

  <!-- HERO IMAGE -->
  <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
      padding-top: 20px;' class='hero'><a target='_blank' style='text-decoration: none;'
      href='https://github.com/konsav/email-templates/'><img border='0' vspace='0' hspace='0'
      src='https://raw.githubusercontent.com/konsav/email-templates/master/images/hero-grid.png'
      alt='Please enable images to view this content' title='Hero Image'
      width='530' style='
      width: 88.33%;
      max-width: 530px;
      color: #FFFFFF; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'/></a></td>
  </tr>

  <!-- PARAGRAPH -->
  <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
      padding-top: 25px; 
      color: #FFFFFF;
      font-family: sans-serif;' class='paragraph'>
        More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;— a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.
    </td>
  </tr>

  <!-- BUTTON -->
  <!-- Set button background color at TD, link/text color at A and TD, font family ('sans-serif' or 'Georgia, serif') at TD. For verification codes add 'letter-spacing: 5px;'. Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
      padding-top: 25px;
      padding-bottom: 35px;' class='button'><a
      href='https://github.com/konsav/email-templates/' target='_blank' style='text-decoration: underline;'>
        <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'><tr><td align='center' valign='middle' style='padding: 12px 24px; margin: 0; text-decoration: underline; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
          bgcolor='#0B5073'><a target='_blank' style='text-decoration: underline;
          color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'
          href='https://github.com/konsav/email-templates/'>
            Explore templates
          </a>
      </td></tr></table></a>
    </td>
  </tr>

<!-- End of WRAPPER -->
</table>

<!-- SECTION / BACKGROUND -->
<!-- Set section background color -->
</td></tr><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
  padding-top: 5px;'
  bgcolor='#FFFFFF'>

<!-- WRAPPER -->
<!-- Set conteiner background color -->
<table border='0' cellpadding='0' cellspacing='0' align='center'
  width='600' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
  max-width: 600px;'>

  <!-- FLOATERS -->
  <!-- Floaters (left align tables) need extra 10px spacing in a line (Outlook fix). Floater table width = (wrapper width / 2) - 10px. Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height. Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 10px; padding-right: 10px;' class='floaters'><table width='280' border='0' cellpadding='0' cellspacing='0' align='left' valign='top' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;' class='floater'><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;
      padding-top: 30px; 
      font-family: sans-serif;
      color: #000000;'><a target='_blank' style='text-decoration: none;
      font-size: 17px; line-height: 160%;'
      href='https://github.com/konsav/email-templates'><img border='0' vspace='0' hspace='0'
      src='https://raw.githubusercontent.com/konsav/email-templates/master/images/hero-grid.png' 
      width='250' height='142'
      alt='Grid Item' title='Grid Item' style='
      color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;' />
        <b style='color:#0B5073; text-decoration: underline;'>Gerenal template</b></a><br/>
        The&nbsp;perfect choice for any purpose of a&nbsp;message.
    </td></tr></table><table width='280' border='0' cellpadding='0' cellspacing='0' align='right' valign='top' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;' class='floater'><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;
      padding-top: 30px; 
      font-family: sans-serif;
      color: #000000;'><a target='_blank' style='text-decoration: none;
      font-size: 17px; line-height: 160%;'
      href='https://github.com/konsav/email-templates'><img border='0' vspace='0' hspace='0'
      src='https://raw.githubusercontent.com/konsav/email-templates/master/images/hero-grid.png' 
      width='250' height='142'
      alt='Grid Item' title='Grid Item' style='
      color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;' />
        <b style='color:#0B5073; text-decoration: underline;'>Transactional template</b></a><br/>
        The&nbsp;one with an&nbsp;action button seen without scrolling.
    </td></tr></table></td>
  </tr>

  <!-- FLOATERS -->
  <!-- Floaters (left align tables) need extra 10px spacing in a line (Outlook fix). Floater table width = (wrapper width / 2) - 10px. Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height. Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. Do not set height for flexible images (including 'auto'). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 10px; padding-right: 10px;' class='floaters'><table width='280' border='0' cellpadding='0' cellspacing='0' align='right' valign='top' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;' class='floater'><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;
      padding-top: 30px;
      font-family: sans-serif;
      color: #000000;'><a target='_blank' style='text-decoration: none;
      font-size: 17px; line-height: 160%;'
      href='https://github.com/konsav/email-templates'><img border='0' vspace='0' hspace='0'
      src='https://raw.githubusercontent.com/konsav/email-templates/master/images/hero-grid.png' 
      width='250' height='142'
      alt='Grid Item' title='Grid Item' style='
      color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;' />
        <b style='color:#0B5073; text-decoration: underline;'>Promotional template</b></a><br/>
        The&nbsp;best for introducing a&nbsp;feature or&nbsp;inviting to an&nbsp;action.
    </td></tr></table><table width='280' border='0' cellpadding='0' cellspacing='0' align='left' valign='top' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;' class='floater'><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;
      padding-top: 30px;
      font-family: sans-serif;
      color: #000000;'><a target='_blank' style='text-decoration: none;
      font-size: 17px; line-height: 160%;'
      href='https://github.com/konsav/email-templates'><img border='0' vspace='0' hspace='0'
      src='https://raw.githubusercontent.com/konsav/email-templates/master/images/hero-grid.png' 
      width='250' height='142'
      alt='Grid Item' title='Grid Item' style='
      color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;' />
        <b style='color:#0B5073; text-decoration: underline;'>Explorational template</b></a><br/>
        Great&nbsp;one for offering a&nbsp;variety of&nbsp;options to&nbsp;explore.
    </td></tr></table></td>
  </tr>

  <!-- BUTTON -->
  <!-- Set button background color at TD, link/text color at A and TD, font family ('sans-serif' or 'Georgia, serif') at TD. For verification codes add 'letter-spacing: 5px;'. Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
      padding-top: 30px;
      padding-bottom: 35px;' class='button'><a
      href='https://github.com/konsav/email-templates/' target='_blank' style='text-decoration: underline;'>
        <table border='0' cellpadding='0' cellspacing='0' align='center' style='max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;'><tr><td align='center' valign='middle' style='padding: 12px 24px; margin: 0; text-decoration: underline; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;'
          bgcolor='#127DB3'><a target='_blank' style='text-decoration: underline;
          color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;'
          href='https://github.com/konsav/email-templates/'>
            Get more templates
          </a>
      </td></tr></table></a>
    </td>
  </tr>

<!-- End of WRAPPER -->
</table>

<!-- SECTION / BACKGROUND -->
<!-- Set section background color -->
</td></tr><tr><td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;'
  bgcolor='#F0F0F0'>

<!-- WRAPPER -->
<!-- Set wrapper width (twice) -->
<table border='0' cellpadding='0' cellspacing='0' align='center'
  width='600' style='border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
  max-width: 600px;' class='wrapper'>

  <!-- SOCIAL NETWORKS -->
  <!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
      padding-top: 25px;' class='social-icons'><table
      width='256' border='0' cellpadding='0' cellspacing='0' align='center' style='border-collapse: collapse; border-spacing: 0; padding: 0;'>
      <tr>

        <!-- ICON 1 -->
        <td align='center' valign='middle' style='margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;'><a target='_blank'
          href='https://raw.githubusercontent.com/konsav/email-templates/'
        style='text-decoration: none;'><img border='0' vspace='0' hspace='0' style='padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          color: #000000;'
          alt='F' title='Facebook'
          width='44' height='44'
          src='https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/facebook.png'></a></td>

        <!-- ICON 2 -->
        <td align='center' valign='middle' style='margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;'><a target='_blank'
          href='https://raw.githubusercontent.com/konsav/email-templates/'
        style='text-decoration: none;'><img border='0' vspace='0' hspace='0' style='padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          color: #000000;'
          alt='T' title='Twitter'
          width='44' height='44'
          src='https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/twitter.png'></a></td>       

        <!-- ICON 3 -->
        <td align='center' valign='middle' style='margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;'><a target='_blank'
          href='https://raw.githubusercontent.com/konsav/email-templates/'
        style='text-decoration: none;'><img border='0' vspace='0' hspace='0' style='padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          color: #000000;'
          alt='G' title='Google Plus'
          width='44' height='44'
          src='https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/googleplus.png'></a></td>    

        <!-- ICON 4 -->
        <td align='center' valign='middle' style='margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;'><a target='_blank'
          href='https://raw.githubusercontent.com/konsav/email-templates/'
        style='text-decoration: none;'><img border='0' vspace='0' hspace='0' style='padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          color: #000000;'
          alt='I' title='Instagram'
          width='44' height='44'
          src='https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/instagram.png'></a></td>

      </tr>
      </table>
    </td>
  </tr>

  <!-- FOOTER -->
  <!-- Set text color and font family ('sans-serif' or 'Georgia, serif'). Duplicate all text styles in links, including line-height -->
  <tr>
    <td align='center' valign='top' style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
      padding-top: 20px;
      padding-bottom: 20px;
      color: #999999;
      font-family: sans-serif;' class='footer'>

        This email template was sent to&nbsp;you becouse we&nbsp;want to&nbsp;make the&nbsp;world a&nbsp;better place.<br/> You&nbsp;could change your <a href='https://github.com/konsav/email-templates/' target='_blank' style='text-decoration: underline; color: #999999; font-family: sans-serif; font-size: 13px; font-weight: 400; line-height: 150%;'>subscription settings</a> anytime.

        <!-- ANALYTICS -->
        <!-- https://www.google-analytics.com/collect?v=1&tid={{UA-Tracking-ID}}&cid={{Client-ID}}&t=event&ec=email&ea=open&cs={{Campaign-Source}}&cm=email&cn={{Campaign-Name}} -->
        <img width='1' height='1' border='0' vspace='0' hspace='0' style='margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;'
        src='https://raw.githubusercontent.com/konsav/email-templates/master/images/tracker.png' />

    </td>
  </tr>

<!-- End of WRAPPER -->
</table>

<!-- End of SECTION / BACKGROUND -->
</td></tr></table>

</body>
</html>";

        public static string Sample5 = @"<!--Download - https://github.com/lime7/responsive-html-template.git-->
<html lang='en'>
<head>
	<meta charset='UTF-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge' />
	<title>One Letter</title>

	<meta name='viewport' content='width=device-width, initial-scale=1.0'/>

	<style>
		.ReadMsgBody {width: 100%; background-color: #ffffff;}
		.ExternalClass {width: 100%; background-color: #ffffff;}

				/* Windows Phone Viewport Fix */
		@-ms-viewport { 
		    width: device-width; 
		}
	</style>

	<!--[if (gte mso 9)|(IE)]>
	    <style type='text/css'>
	        table {border-collapse: collapse;}
	        .mso {display:block !important;} 
	    </style>
	<![endif]-->

</head>
<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' style='background: #e7e7e7; width: 100%; height: 100%; margin: 0; padding: 0;'>
	<!-- Mail.ru Wrapper -->
	<div id='mailsub'>
		<!-- Wrapper -->
		<center class='wrapper' style='table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; padding: 0; margin: 0 auto; width: 100%; max-width: 960px;'>
			<!-- Old wrap -->
	        <div class='webkit'>
				<table cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' style='padding: 0; margin: 0 auto; width: 100%; max-width: 960px;'>
					<tbody>
						<tr>
							<td align='center'>
								<!-- Start Section (1 column) -->
								<table id='intro' cellpadding='0' cellspacing='0' border='0' bgcolor='#4F6331' align='center' style='width: 100%; padding: 0; margin: 0; background-image: url(https://github.com/lime7/responsive-html-template/blob/master/index/intro__bg.png?raw=true); background-size: auto 102%; background-position: center center; background-repeat: no-repeat; background-color: #080e02'>
									<tbody >
										<tr><td colspan='3' height='20'></td></tr>
										<tr>
											<td width='330' style='width: 33%;'></td>
											<!-- Logo -->
											<td width='300' style='width: 30%;' align='center'>
												<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 60px; height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif;  -webkit-text-size-adjust:none;'>
													<img src='https://github.com/lime7/responsive-html-template/blob/master/index/logo.png?raw=true' alt='One Letter' width='193' height='43' border='0' style='border: none; display: block; -ms-interpolation-mode: bicubic;'>
												</a>
											</td>
											<!-- Social Button -->
											<td width='330' style='width: 33%;' align='right'>
												<div style='text-align: center; max-width: 150px; width: 100%;'>
													<span>&nbsp;</span>
													<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif;  -webkit-text-size-adjust:none'>
														<img src='https://github.com/lime7/responsive-html-template/blob/master/index/f.png?raw=true' alt='facebook.com' border='0' width='11' height='23' style='border: none; outline: none; -ms-interpolation-mode: bicubic;'>
													</a>
													<span>&nbsp;</span>
													<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none'>
														<img src='https://github.com/lime7/responsive-html-template/blob/master/index/vk.png?raw=true' alt='vk.com' border='0' width='39' height='23' style='border: none; outline: none; -ms-interpolation-mode: bicubic;'>
													</a>
													<span>&nbsp;</span>
													<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>
														<img src='https://github.com/lime7/responsive-html-template/blob/master/index/g+.png?raw=true' alt='google.com' border='0' width='23' height='23' style='border: none; outline: none; -ms-interpolation-mode: bicubic;'>
													</a>
													<span>&nbsp;</span>
												</div>
											</td>
										</tr>
										<tr><td colspan='3' height='100'></td></tr>
										<!-- Main Title -->
										<tr>
											<td colspan='3' height='60' align='center'>
												<div border='0' style='border: none; line-height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 52px; text-transform: uppercase; font-weight: bolder;'>HELLO, WORLD!</div>
											</td>
										</tr>
										<!-- Line 1 -->
										<tr>
											<td colspan='3' height='20' valign='bottom' align='center'>
												<img src='https://github.com/lime7/responsive-html-template/blob/master/index/line-1.png?raw=true' alt='line' border='0' width='464' height='5' style='border: none; outline: none; max-width: 464px; width: 100%; -ms-interpolation-mode: bicubic;' >
											</td>
										</tr>
										<!-- Meta title -->
										<tr>
											<td colspan='3'>
												<table cellpadding='0' cellspacing='0' border='0' align='center' style='padding: 0; margin: 0; width: 100%;'>
													<tbody>
														<tr>
															<td width='90' style='width: 9%;'></td>
															<td align='center'>
																<div border='0' style='border: none; height: 60px;'>
																	<p style='font-size: 18px; line-height: 24px; font-family: Verdana, Geneva, sans-serif; color: #ffffff; text-align: center; mso-table-lspace:0;mso-table-rspace:0;'>
																		This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.
																	</p>
																</div>
															</td>
															<td width='90' style='width: 9%;'></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr><td colspan='3' height='160'></td></tr>
										<tr>
											<td width='330'></td>
											<!-- Button Start -->
											<td width='300' align='center' height='52'>
												<div style='background-image: url(https://github.com/lime7/responsive-html-template/blob/master/index/intro__btn.png?raw=true); background-size: 100% 100%; background-position: center center; width: 225px;'>
													<a href='#' target='_blank' width='160' height='52' border='0' bgcolor='#009789' style='border: none; outline: none; display: block; width:160px; height: 52px; text-transform: uppercase; text-decoration: none; font-size: 17px; line-height: 52px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; text-align: center; background-color: #009789;  -webkit-text-size-adjust:none;'>
														Get it now
													</a>
												</div>
											</td>
											<td width='330'></td>
										</tr>
										<tr><td colspan='3' height='85'></td></tr>
									</tbody>
								</table><!-- End Start Section -->
								<!-- Icon articles (4 columns) -->
								<table id='icon__article' class='device' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center' style='width: 100%; padding: 0; margin: 0; background-color: #ffffff'>
									<tbody>								
										<tr>
											<td align='center'>
												<div style='display: inline-block;'>
													<table width='240' align='left' cellpadding='0' cellspacing='0' border='0' style='padding: 0; margin: 0; mso-table-lspace:0pt; mso-table-rspace:0pt;'  class='article'>
													<tbody>
														<tr> <td colspan='3' height='40'></td> </tr>
														<tr>
															<td width='80' style='width: 8%;'></td>
															<td align='center'>
																<div class='imgs'>
																	<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 60px; height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; margin: 0 30px; -webkit-text-size-adjust:none;'>
																		<img src='https://github.com/lime7/responsive-html-template/blob/master/index/ico-1.png?raw=true' alt='icon' border='0' width='60' height='60' style='border: none; display: block; outline: none; -ms-interpolation-mode: bicubic;'>
																	</a>
																</div>
																<h3 border='0' style='border: none; line-height: 14px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-transform: uppercase; font-weight: normal; overflow: hidden; margin:17px 0 0px 0;'>Lorem Ipsum
																</h3>
																<p style='line-height: 20px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-align: center; overflow: hidden; margin: 10px 0; mso-table-lspace:0;mso-table-rspace:0;'> This is Photoshop's version  of Lorem
																</p>
															</td>
															<td width='80' style='width: 8%;'></td>
														</tr>
														<tr><td colspan='3' height='10'></td></tr>					
														<tr>
															<td colspan='3' height='5' valign='top' align='center'>
																<img src='https://github.com/lime7/responsive-html-template/blob/master/index/line-2.png?raw=true' alt='line' border='0' width='960' height='5' style='border: none; outline: none; max-width: 960px; width: 100%; -ms-interpolation-mode: bicubic;' >
															</td>
														</tr>						
													</tbody>
													</table>
												</div>

												<div style='display: inline-block; margin-left: -4px;'>
													<table width='240' align='left' cellpadding='0' cellspacing='0' border='0' style='padding: 0; margin: 0; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='article'>
													<tbody>
														<tr> <td colspan='3' height='40'></td> </tr>
														<tr>
															<td width='80' style='width: 8%;'></td>
															<td align='center'>
																<div class='imgs'>
																	<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 60px; height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; margin: 0 30px; -webkit-text-size-adjust:none;'>
																		<img src='https://github.com/lime7/responsive-html-template/blob/master/index/ico-2.png?raw=true' alt='icon' border='0' width='60' height='60' style='border: none; display: block; outline: none; -ms-interpolation-mode: bicubic;'>
																	</a>
																</div>
																<h3 border='0' style='border: none; line-height: 14px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-transform: uppercase; font-weight: normal; overflow: hidden; margin:17px 0 0px 0;'>Lorem Ipsum
																</h3>
																<p style='line-height: 20px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-align: center; overflow: hidden; margin: 10px 0; mso-table-lspace:0;mso-table-rspace:0;'> This is Photoshop's version  of Lorem
																</p>
															</td>
															<td width='80' style='width: 8%;'></td>
														</tr>
														<tr><td colspan='3' height='10'></td></tr>					
														<tr>
															<td colspan='3' height='5' valign='top' align='center'>
																<img src='https://github.com/lime7/responsive-html-template/blob/master/index/line-2.png?raw=true' alt='line' border='0' width='960' height='5' style='border: none; outline: none; max-width: 960px; width: 100%; -ms-interpolation-mode: bicubic;' >
															</td>
														</tr>					
													</tbody>
													</table>
												</div>

												<div style='display: inline-block; margin-left: -4px;'>
													<table width='240' align='left' cellpadding='0' cellspacing='0' border='0' style='padding: 0; margin: 0; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='article'>
													<tbody>
														<tr> <td colspan='3' height='40'></td> </tr>
														<tr>
															<td width='80' style='width: 8%;'></td>
															<td align='center'>
																<div class='imgs'>
																	<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 60px; height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; margin: 0 30px; -webkit-text-size-adjust:none;'>
																		<img src='https://github.com/lime7/responsive-html-template/blob/master/index/ico-3.png?raw=true' alt='icon' border='0' width='60' height='60' style='border: none; display: block; outline: none; -ms-interpolation-mode: bicubic;'>
																	</a>
																</div>
																<h3 border='0' style='border: none; line-height: 14px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-transform: uppercase; font-weight: normal; overflow: hidden; margin:17px 0 0px 0;'>Lorem Ipsum
																</h3>
																<p style='line-height: 20px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-align: center; overflow: hidden; margin: 10px 0; mso-table-lspace:0;mso-table-rspace:0;'> This is Photoshop's version  of Lorem
																</p>
															</td>
															<td width='80' style='width: 8%;'></td>
														</tr>
														<tr><td colspan='3' height='10'></td></tr>					
														<tr>
															<td colspan='3' height='5' valign='top' align='center'>
																<img src='https://github.com/lime7/responsive-html-template/blob/master/index/line-2.png?raw=true' alt='line' border='0' width='960' height='5' style='border: none; outline: none; max-width: 960px; width: 100%; -ms-interpolation-mode: bicubic;' >
															</td>
														</tr>					
													</tbody>
													</table>
												</div>

												<div style='display: inline-block; margin-left: -4px;'>
													<table width='240' align='left' cellpadding='0' cellspacing='0' border='0' style='padding: 0; margin: 0; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='article'>
													<tbody>
														<tr> <td colspan='3' height='40'></td> </tr>
														<tr>
															<td width='80' style='width: 8%;'></td>
															<td align='center'>
																<div class='imgs'>
																	<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 60px; height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; margin: 0 30px; -webkit-text-size-adjust:none;'>
																		<img src='https://github.com/lime7/responsive-html-template/blob/master/index/ico-4.png?raw=true' alt='icon' border='0' width='60' height='60' style='border: none; display: block; outline: none; -ms-interpolation-mode: bicubic;'>
																	</a>
																</div>
																<h3 border='0' style='border: none; line-height: 14px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-transform: uppercase; font-weight: normal; overflow: hidden; margin:17px 0 0px 0;'>Lorem Ipsum
																</h3>
																<p style='line-height: 20px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-align: center; overflow: hidden; margin: 10px 0; mso-table-lspace:0;mso-table-rspace:0;'> This is Photoshop's version  of Lorem
																</p>
															</td>
															<td width='80' style='width: 8%;'></td>
														</tr>
														<tr><td colspan='3' height='10'></td></tr>					
														<tr>
															<td colspan='3' height='5' valign='top' align='center'>
																<img src='https://github.com/lime7/responsive-html-template/blob/master/index/line-2.png?raw=true' alt='line' border='0' width='960' height='5' style='border: none; outline: none; max-width: 960px; width: 100%; -ms-interpolation-mode: bicubic; ' >
															</td>
														</tr>					
													</tbody>
													</table>
												</div>										
											</td>
										</tr>
										<tr> <td colspan='5' height='40'></td> </tr>
									</tbody>
								</table> <!-- End Icon articles -->						
								<!-- News article (2 columns) -->
								<table id='news__article' class='device' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center' style='width: 100%; padding: 0; margin: 0; background-color: #ffffff'>
									<tbody>
										<tr>
											<td width='90' style='width: 9.3%;'></td>
											<td align='center'>
												<div style='display: inline-block;'>
													<table class='news__art' align='center' cellpadding='5' cellspacing='0' border='0' bgcolor='#ffffff' style='padding: 0; margin: 0; mso-table-lspace:0pt; mso-table-rspace:0pt;'>
														<tbody>
															<tr>
																<td width='80' style='width:10%;'></td>
																<td colspan='2' align='center' width='300'>
																	<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 220px; height: 220px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>
																		<img src='https://github.com/lime7/responsive-html-template/blob/master/index/pic-1.png?raw=true' alt='icon' border='0' width='300' height='220' style='border: none; display: block; outline: none; -ms-interpolation-mode: bicubic;'>
																	</a>
																</td>
																<td width='80' style='width:10%;'></td>
															</tr>
															<tr> <td colspan='4' height='15'></td> </tr>
															<tr>
																<td width='80' style='width:10%;'></td>
																<td align='left'>
																	<div border='0' style='border: none; line-height: 22px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 16px;'>
																		Lorem Ipsum
																	</div>
																</td>
																<td align='right'>
																	<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 18px; font-size: 12px; color: #b7b7b7; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>21 aug</a>
																	<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
																	<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 18px; font-size: 12px; color: #b7b7b7; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>Author</a>
																</td>
																<td width='80' style='width:10%;'></td>
															</tr>
															
															<tr>
																<td width='80' style='width:10%;'></td>
																<td colspan='2' width='300'>
																	<div border='0' style='border: none; line-height: 18px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 13px;'>
																		This is Photoshop's version  of Lorem Ipsum. Proin <a href='#' target='_blank' border='0' style='border: none; outline: none; line-height: 18px; font-size: 13px; color: #31796e; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>more ... </a>
																	</div>
																</td>
																<td width='80' style='width:10%;'></td>
															</tr>
															<tr> <td colspan='4' height='40'></td> </tr>
														</tbody>
													</table>
												</div>

												<div style='display: inline-block;'>
													<table class='news__art' align='center' cellpadding='5' cellspacing='0' border='0' bgcolor='#ffffff' style='padding: 0; margin: 0; mso-table-lspace:0pt; mso-table-rspace:0pt; '>
														<tbody>
															<tr>
																<td width='80' style='width:10%;'></td>
																<td colspan='2' align='center' width='300'>
																	<a href='#' target='_blank' border='0' style='border: none; display: block; outline: none; text-decoration: none; line-height: 220px; height: 220px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>
																		<img src='https://github.com/lime7/responsive-html-template/blob/master/index/pic-2.png?raw=true' alt='icon' border='0' width='300' height='220' style='border: none; display: block; outline: none; -ms-interpolation-mode: bicubic;'>
																	</a>
																</td>
																<td width='80' style='width:10%;'></td>
															</tr>
															<tr> <td colspan='4' height='15'></td> </tr>
															<tr>
																<td width='80' style='width:10%;'></td>
																<td align='left'>
																	<div border='0' style='border: none; line-height: 22px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 16px;'>
																		Lorem Ipsum
																	</div>
																</td>
																<td align='right'>
																	<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 18px; font-size: 12px; color: #b7b7b7; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>21 aug</a>
																	<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
																	<a href='#' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 18px; font-size: 12px; color: #b7b7b7; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>Author</a>
																</td>
																<td width='80' style='width:10%;'></td>
															</tr>
															
															<tr>
																<td width='80' style='width:10%;'></td>
																<td colspan='2' width='300'>
																	<div border='0' style='border: none; line-height: 18px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 13px;'>
																		This is Photoshop's version  of Lorem Ipsum. Proin <a href='#' target='_blank' border='0' style='border: none; outline: none; line-height: 18px; font-size: 13px; color: #31796e; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>more ... </a>
																	</div>
																</td>
																<td width='80' style='width:10%;'></td>
															</tr>
															<tr> <td colspan='4' height='40'></td> </tr>
														</tbody>
													</table>
												</div>
											</td>
											<td width='90' style='width: 9.3%;'></td>
										</tr>
									</tbody>
								</table> <!-- End News article -->						
								<!-- One art (2 columns) -->
								<table id='one__art' class='device' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center' style='width: 100%; padding: 0; margin: 0; background-image: url(https://github.com/lime7/responsive-html-template/blob/master/index/one__bg.png?raw=true); background-size: 100% 100%; background-position: center center; background-repeat: no-repeat; background-color: #ECECED;'>
									<tbody>
										<tr>											
											<td align='center'>
												<table align='right' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' style='padding: 0; margin: 0; background-image: url(ohttps://github.com/lime7/responsive-html-template/blob/master/index/one__art-mask.png?raw=true); background-size: 100% 100%; background-position: center center; background-repeat: no-repeat; background-color: rgba(255,255,255,.85); max-width: 480px; width:100%;'>
													<tbody>
														<tr><td colspan='3' height='15'></td></tr>
														<tr>
															<td width='20' style='width:4%;'></td>
															<td align='left'>
																<div border='0' style='border: none; line-height: 18px; color: #212121; font-family: Verdana, Geneva, sans-serif; font-size: 20px;'>
																	Lorem Ipsum
																</div>
															</td>
															<td width='20' style='width:4%;'></td>
														</tr>
														<tr>
															<td width='20' style='width:4%;'></td>
															<td align='left'>
																<div border='0' style='border: none;'>
																	<p style='line-height: 22px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px; text-align: left; mso-table-lspace:0;mso-table-rspace:0;'>
																		This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
																	</p>
																</div>
															</td>
															<td width='20' style='width:4%;'></td>
														</tr>
														<tr>
															<td width='20' style='width:4%;'></td>
															<td align='left' height='34'>
																<div style='background-image: url(https://github.com/lime7/responsive-html-template/blob/master/index/one__art-btn.png?raw=true); background-size: 100% 100%; background-position: center center; width: 125px;'>
																	<a href='#' target='_blank' width='90' height='34' align='center' bgcolor='#9de0dc' border='0' style='border: none; outline: none; display: block; height: 34px; text-decoration: none; font-size: 16px; line-height: 34px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; text-align: center;  -webkit-text-size-adjust:none; background-color:#9de0dc; margin: 0 auto; width: 90px;'>
																		view more
																	</a>
																</div>
															</td>
															<td width='20' style='width:4%;'></td>
														</tr>
														<tr><td colspan='3' height='13'></td></tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table> <!-- End One art -->
								<!-- Footer -->
								<table id='news__article' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center' style='width: 100%; padding: 0; margin: 0; background-color: #ffffff'>
									<tbody>
										<tr><td colspan='3' height='23'></td></tr>
										<tr>
											<td align='center'>
												<div border='0' style='border: none; line-height: 14px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px;'>
													2015 © <a href='https://semenchenkov.github.io/' target='_blank' border='0' style='border: none; outline: none; text-decoration: none; line-height: 14px; font-size: 16px; color: #727272; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none;'>Semenchenko V. Helen</a>
												</div>
											</td>
										</tr>
										<tr><td colspan='3' height='23'></td></tr>
									</tbody>
								</table> <!-- End Footer -->
							</td>
						</tr>
					</tbody>
				</table>
			</div> <!-- End Old wrap -->
		</center> <!-- End Wrapper -->
	</div> <!-- End Mail.ru Wrapper -->
</body>

</html>";


        public static string Sample6 = @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
<title>A Simple Responsive HTML Email</title>
<style type='text/css'>
body {margin: 0; padding: 0; min-width: 100%!important;}
img {height: auto;}
.content {width: 100%; max-width: 600px;}
.header {padding: 40px 30px 20px 30px;}
.innerpadding {padding: 30px 30px 30px 30px;}
.borderbottom {border-bottom: 1px solid #f2eeed;}
.subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
.h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
.h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
.h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
.bodycopy {font-size: 16px; line-height: 22px;}
.button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
.button a {color: #ffffff; text-decoration: none;}
.footer {padding: 20px 30px 15px 30px;}
.footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
.footercopy a {color: #ffffff; text-decoration: underline;}
@media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
body[yahoo] .hide {display: none!important;}
body[yahoo] .buttonwrapper {background-color: transparent!important;}
body[yahoo] .button {padding: 0px!important;}
body[yahoo] .button a {background-color: #effb41; padding: 15px 15px 13px!important;}
body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
}
/*@media only screen and (min-device-width: 601px) {
.content {width: 600px !important;}
.col425 {width: 425px!important;}
.col380 {width: 380px!important;}
}*/
</style>
</head>
<body yahoo bgcolor='#ffffff'>
<table width='100%' bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0'>
<tr>
  <td>
    <!--[if (gte mso 9)|(IE)]>
      <table width='600' align='center' cellpadding='0' cellspacing='0' border='0'>
      <tr>
      <td>
      <![endif]-->
    <table bgcolor='#ffffff' class='content' align='center' cellpadding='0' cellspacing='0' border='0'>
    <tr>
      <td bgcolor='#00bcd4' class='header'>
        <table width='70' align='left' border='0' cellpadding='0' cellspacing='0'>
        <tr>
          <td height='70' style='padding: 0 20px 20px 0;'>
            <img class='fix' src='http://placehold.it/70x70' width='70' height='70' border='0' alt=''/>
          </td>
        </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
          <table width='425' align='left' cellpadding='0' cellspacing='0' border='0'>
          <tr>
          <td>
          <![endif]-->
        <table class='col425' align='left' border='0' cellpadding='0' cellspacing='0' style='width: 100%; max-width: 425px;'>
        <tr>
          <td height='70'>
            <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
              <td class='subhead' style='padding: 0 0 0 3px;'>
                 SOME
              </td>
            </tr>
            <tr>
              <td class='h1' style='padding: 5px 0 0 0;'>
                 RESPONSIVE MAIL
              </td>
            </tr>
            </table>
          </td>
        </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
          </td>
          </tr>
          </table>
          <![endif]-->
      </td>
    </tr>
    <tr>
      <td class='innerpadding borderbottom'>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
        <tr>
          <td class='h2'>
             LOREM IPSUM
          </td>
        </tr>
        <tr>
          <td class='bodycopy'>
             Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
          </td>
        </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class='innerpadding borderbottom'>
        <table width='115' align='left' border='0' cellpadding='0' cellspacing='0'>
        <tr>
          <td height='115' style='padding: 0 20px 20px 0;'>
            <img class='fix' src='http://placehold.it/115x115' width='115' height='115' border='0' alt=''/>
          </td>
        </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
          <table width='380' align='left' cellpadding='0' cellspacing='0' border='0'>
          <tr>
          <td>
          <![endif]-->
        <table class='col380' align='left' border='0' cellpadding='0' cellspacing='0' style='width: 100%; max-width: 380px;'>
        <tr>
          <td>
            <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
              <td class='bodycopy'>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
              </td>
            </tr>
            <tr>
              <td style='padding: 20px 0 0 0;'>
                <table class='buttonwrapper' bgcolor='#effb41' border='0' cellspacing='0' cellpadding='0'>
                <tr>
                  <td class='button' height='45'>
                    <a href='#'>CTA</a>
                  </td>
                </tr>
                </table>
              </td>
            </tr>
            </table>
          </td>
        </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
          </td>
          </tr>
          </table>
          <![endif]-->
      </td>
    </tr>
    <tr>
      <td class='innerpadding borderbottom'>
        <img class='fix' src='http://placehold.it/600x200' width='100%' border='0' alt=''/>
      </td>
    </tr>
    <tr>
      <td class='innerpadding bodycopy'>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
      </td>
    </tr>
    <tr>
      <td class='footer' bgcolor='#44525f'>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
        <tr>
          <td align='center' class='footercopy'>
             &reg; Someone, somewhere 2013<br/>
            <a href='#' class='unsubscribe'><font color='#ffffff'>Unsubscribe</font></a>
            <span class='hide'>from this newsletter instantly</span>
          </td>
        </tr>
        <tr>
          <td align='center' style='padding: 20px 0 0 0;'>
            <table border='0' cellspacing='0' cellpadding='0'>
            <tr>
              <td width='37' style='text-align: center; padding: 0 10px 0 10px;'>
                <a href='https://www.facebook.com/'>
                <img src='http://placehold.it/37x37' width='37' height='37' alt='Facebook' border='0'/>
                </a>
              </td>
              <td width='37' style='text-align: center; padding: 0 10px 0 10px;'>
                <a href='https://www.twitter.com/'>
                <img src='http://placehold.it/37x37' width='37' height='37' alt='Twitter' border='0'/>
                </a>
              </td>
            </tr>
            </table>
          </td>
        </tr>
        </table>
      </td>
    </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
  </td>
</tr>
</table>
</body>";


        public static string Sample7 = @"
<style>
    @media only screen and (max-width: 600px) {
		.main {
			width: 320px !important;
		}

		.top-image {
			width: 100% !important;
		}
		.inside-footer {
			width: 320px !important;
		}
		table[class='contenttable'] { 
            width: 320px !important;
            text-align: left !important;
        }
        td[class='force-col'] {
	        display: block !important;
	    }
	     td[class='rm-col'] {
	        display: none !important;
	    }
		.mt {
			margin-top: 15px !important;
		}
		*[class].width300 {width: 255px !important;}
		*[class].block {display:block !important;}
		*[class].blockcol {display:none !important;}
		.emailButton{
            width: 100% !important;
        }

        .emailButton a {
            display:block !important;
            font-size:18px !important;
        }

	}
</style>

  <body link='#00a5b5' vlink='#00a5b5' alink='#00a5b5'>

    <table class=' main contenttable' align='center' style='font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;'>
            <tr>
                <td class='border' style='border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'>
                    <table style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;'>
                        <tr>
                            <td colspan='4' valign='top' class='image-section' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5'>
                                <a href='https://tenable.com'><img class='top-image' src='https://info.tenable.com/rs/tenable/images/tenable-white-email.png' style='line-height: 1;width: 600px;' alt='Tenable Network Security'></a>
                            </td>
                        </tr>
                        <tr>
                            <td valign='top' class='side title' style='border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;'>
                                <table style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;'>
                                    <tr>
                                        <td class='head-title' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;'>
                                            <div class='mktEditable' id='main_title'>
                                                Title of Email is super long and detailed
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='sub-title' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;padding-top:5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 18px;line-height: 29px;font-weight: bold;text-align: center;'>
                                        <div class='mktEditable' id='intro_title'>
                                            Sub title:
                                        </div></td>
                                    </tr>
                                    <tr>
                                        <td class='top-padding' style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'></td>
                                    </tr>
                                    <tr>
                                        <td class='grey-block' style='border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:center;'>
                                        <div class='mktEditable' id='cta'>
                                        <img class='top-image' src='https://info.tenable.com/rs/tenable/images/webinar-no-text.png' width='560'/><br><br>
                                            <strong>Date:</strong> Caecuss, Dabico xx, XXXX<br>
                                            <strong>Time</strong>: 9:00 am &#8211; 4:00 pm<br><br>
                                             <a style='color:#ffffff; background-color: #ff8300;  border: 10px solid #ff8300; border-radius: 3px; text-decoration:none;' href='#'>Download Now</a>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='top-padding' style='border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;'>
                                            <hr size='1' color='#eeeff0'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='text' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'>
                                        <div class='mktEditable' id='main_text'>
                                            Hello {{lead.First Name:default=Sir/Madam}},<br><br>
    
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;'>
                                         &nbsp;<br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='text' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;'>
                                        <div class='mktEditable' id='download_button' style='text-align: center;'>
                                            <a style='color:#ffffff; background-color: #ff8300; border: 20px solid #ff8300; border-left: 20px solid #ff8300; border-right: 20px solid #ff8300; border-top: 10px solid #ff8300; border-bottom: 10px solid #ff8300;border-radius: 3px; text-decoration:none;' href='#'>Download Now</a>										
                                        </div>
                                        </td>
                                    </tr>
    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style='padding:20px; font-family: Arial, sans-serif; -webkit-text-size-adjust: none;' align='center'>
                                <table>
                                    <tr>
                                        <td align='center' style='font-family: Arial, sans-serif; -webkit-text-size-adjust: none; font-size: 16px;'>
                                            <a style='color: #00a5b5;' href='{{system.forwardToFriendLink}}'>Forward this Email</a>
                                            <br/><span style='font-size:10px; font-family: Arial, sans-serif; -webkit-text-size-adjust: none;' >Please only forward this email to colleagues or contacts who will be interested in receiving this email.</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px; padding: 20px;'>
                            <div class='mktEditable' id='cta_try'>
                                <table border='0' cellpadding='0' cellspacing='0' class='mobile' style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;'>
                                    <tr>
                                        <td class='force-col' valign='top' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;'>
                                        
                                            <table class='mb mt' style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;margin-bottom: 15px;margin-top: 0;'>
                                                <tr>
                                                    <td class='grey-block' style='border-collapse: collapse;border: 0;margin: 0;padding: 18px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;background-color: #fff; border-top: 3px solid #00a5b5; border-left: 1px solid #E6E6E6; border-right: 1px solid #E6E6E6; border-bottom: 1px solid #E6E6E6; width: 250px; text-align: center;'>
                                                    
                                                    <span style='font-family: Arial, sans-serif; font-size: 24px; line-height: 39px; border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559; text-align: center;font-weight: bold;'>Try Our Products</span><br>
                                                    Get started with a trial for your organization<br><br>
                                                                                                             <a style='color:#ffffff; background-color: #00a5b5;  border-top: 10px solid #00a5b5; border-bottom: 10px solid #00a5b5; border-left: 20px solid #00a5b5; border-right: 20px solid #00a5b5; border-radius: 3px; text-decoration:none;' href='https://www.tenable.com/evaluate'>Try Now</a>
                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>	
                                        <td class='rm-col' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;padding-right: 15px;'></td>
                                        <td class='force-col' valign='top' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;'>
                                            <table class='mb mt' style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;margin-bottom: 15px;margin-top: 0;'>
                                                <tr>
                                                    <td class='grey-block' style='border-collapse: collapse;border: 0;margin: 0;padding: 18px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 24px;background-color: #fff; border-top: 3px solid #00a5b5; border-left: 1px solid #E6E6E6; border-right: 1px solid #E6E6E6; border-bottom: 1px solid #E6E6E6; width: 250px; text-align: center;'>
                                                    
                                                    <span style='font-family: Arial, sans-serif; font-size: 24px; line-height: 39px; border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559; text-align: center;font-weight: bold;'>Buy Our Products</span><br>
                                                    Get the full power of Tenable working for you<br><br>
                                                                                                             <a style='color:#ffffff; background-color: #00a5b5;  border-top: 10px solid #00a5b5; border-bottom: 10px solid #00a5b5; border-left: 20px solid #00a5b5; border-right: 20px solid #00a5b5; border-radius: 3px; text-decoration:none;' href='https://www.tenable.com/products/buy'>Buy Now</a>
                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>	
                                    </tr>
                                </table>
                            </div>
                            </td>
                        </tr>											
                        <tr>
                            <td valign='top' align='center' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'>
                                <table style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;'>
                                    <tr>
                                        <td align='center' valign='middle' class='social' style='border-collapse: collapse;border: 0;margin: 0;padding: 10px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;text-align: center;'>
                                            <table style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;'>
                                                <tr>
                                                    <td style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'><a href='https://www.tenable.com/blog'><img src='https://info.tenable.com/rs/tenable/images/rss-teal.png'></a></td>
                                        <td style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'><a href='https://twitter.com/tenablesecurity'><img src='https://info.tenable.com/rs/tenable/images/twitter-teal.png'></a></td>
                                        <td style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'><a href='https://www.facebook.com/Tenable.Inc'><img src='https://info.tenable.com/rs/tenable/images/facebook-teal.png'></a></td>
                                        <td style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'><a href='https://www.youtube.com/tenablesecurity'><img src='https://info.tenable.com/rs/tenable/images/youtube-teal.png'></a></td>
                                        <td style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'><a href='https://www.linkedin.com/company/tenable-network-security'><img src='https://info.tenable.com/rs/tenable/images/linkedin-teal.png'></a></td>
                                        <td style='border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;'><a href='https://plus.google.com/107158297098429070217'><img src='https://info.tenable.com/rs/tenable/images/google-teal.png'></a></td>
    
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor='#fff' style='border-top: 4px solid #00a5b5;'>
                            <td valign='top' class='footer' style='border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;'>
                                <table style='font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;'>
                                    <tr>
                                        <td class='inside-footer' align='center' valign='middle' style='border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;'>
    <div id='address' class='mktEditable'>
                                            <b>Tenable Network Security</b><br>
                                7021 Columbia Gateway Drive<br>  Suite 500 <br> Columbia, MD 21046<br>
                                <a style='color: #00a5b5;' href='https://www.tenable.com/contact-tenable'>Contact Us</a>
    </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
      </body>";
    }
}