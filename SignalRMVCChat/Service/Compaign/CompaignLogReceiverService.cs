﻿using TelegramBotsWebApplication.Areas.Admin.Service;

namespace SignalRMVCChat.Service.Compaign
{
    public class CompaignLogReceiverService: GenericService<Models.Compaign.CompaignLogReceiver>
    {
        public CompaignLogReceiverService() : base(null)
        {
        }
    }
}