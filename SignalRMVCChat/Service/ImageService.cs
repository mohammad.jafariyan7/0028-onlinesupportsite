﻿using SignalRMVCChat.Models;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace SignalRMVCChat.Service
{
    public class ImageService:GenericService<Image>
    {
        public ImageService() : base(null)
        {
        }
        
    }
}