﻿using TelegramBotsWebApplication.Areas.Admin.Service;

namespace SignalRMVCChat.Service.ReadyPm
{
    public class ReadyPmService:GenericService<Models.ReadyPm.ReadyPm>
    {
        public ReadyPmService() : base(null)
        {
        }
    }
}