﻿using TelegramBotsWebApplication.Areas.Admin.Service;

namespace SignalRMVCChat.Service.RemindMe
{
    public class RemindMeService: GenericService<Models.RemindMe.RemindMe>
    {
        public RemindMeService() : base(null)
        {
        }
    }
}