﻿using TelegramBotsWebApplication.Areas.Admin.Service;

public class UserCity
{
    public string name { get; set; }
    public string engName { get; set; }

    public string Id { get; set; }

    public string section_id { get; set; }
}