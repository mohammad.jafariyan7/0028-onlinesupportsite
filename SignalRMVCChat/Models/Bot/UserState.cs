﻿using TelegramBotsWebApplication.Areas.Admin.Service;

public class UserState:BaseEntity
{
    public string name { get; set; }
    public string engName { get; set; }
}