﻿namespace SignalRMVCChat.Models.Compaign
{
    public class CompaignHtmlTemplate
    {
        public string Name { get; set; }
        public string Html { get; set; }
    }
}